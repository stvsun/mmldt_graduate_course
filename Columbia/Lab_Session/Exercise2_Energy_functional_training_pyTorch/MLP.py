import torch
import torch.nn as nn

class MLP(nn.Module):
    def __init__(self, size=[3,32,32,1]):
        super(MLP,self).__init__()
        # self.conv1 = nn.Conv2d(in_channels=1, out_channels=32, kernel_size=3)
        self.d1 = nn.Linear(size[0],size[1])
        self.act1 = nn.ELU(alpha=1)
        self.d2 = nn.Linear(size[1],size[2])
        self.act2 = nn.ELU(alpha=1)
        self.d3 = nn.Linear(size[2],size[3])

    def forward(self,x):
        x = self.d1(x)
        x = self.act1(x)
        x = self.d2(x)
        x = self.act2(x)
        out = self.d3(x)
        return out